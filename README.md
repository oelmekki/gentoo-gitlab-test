# Gentoo Gitlab test

This docker image is meant to be a test for checking if Gitlab's GDK is
installing properly on Gentoo. This is part of the [Discuss supporting
another platform in
GDK](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/229)
discussion.

The idea is to pull the latest Gentoo image, then install dependencies with
the asdf based script, install GDK and Gitlab, and run `gdk doctor` to see
if it passes.

That way, we can automate checking if this installation method on that
platform still work without waiting for users to report it.
